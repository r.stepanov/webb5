<?php
    session_start();
    unset($_SESSION["count"]);
    setcookie("admin","",time());
?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style1.css">
    <title>MY site</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script>
        function next_page_1(){
            window.location.href = "enter.php";
            return false;
        }

        function next_page_2(){
            window.location.href = "registration.php";
            return false;
        }

        function next_page_3(){
            window.location.href = "admin.php";
            return false;
        }
        
        function Master(){
            window1.onclick=next_page_1;
            window2.onclick=next_page_2;
            window4.onclick=next_page_3;
            return false;
        }

        document.addEventListener("DOMContentLoaded",Master);
    </script>
</head>
<body>
    <div class="main">
        <div id="window1" class="window">
            <div class="big-text">ВОЙТИ</div>
            <div class="line"></div>
        </div>
        <div id="window2" class="window">
            <div class="big-text">РЕГИСТРАЦИЯ</div>
            <div class="line"></div>
        </div>
        <div id="window4" class="window">
            <div class="big-text">СТРАНИЦА<br>АДМИНИСТРАТОРА</div>
            <div class="line"></div>
        </div>
    </div>
</body>
</html>

