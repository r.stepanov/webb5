<?php

$db = new PDO('mysql:host=localhost;dbname=u21139', 'u21139', '2017773', array(PDO::ATTR_PERSISTENT => true));

if(empty($_GET["radio1"])){
    $name="";
    $password="";
    $email="";
    $date="";

    $error_check="";
    $error_date="";
    $error_password1="";
    $error_password2="";
    $error_email1="";
    $error_email2="";
    $error_name1="";
    $error_name2="";
    $check_gen1="checked";
    $check_gen2="";
    $check_lim = array("","","","","checked");
    $text="";
    $answer="";
    $answer_style="";
    $good=0;
}
else{
    $good=0;

    if(!empty($_GET["name"])){
    $name = htmlentities($_GET["name"]);
    $res = $db->prepare("SELECT name FROM info WHERE name='$name'");
    $res->execute();
    $db_name = $res->fetch(PDO::FETCH_NUM);
    if(!$db_name){
        $num = strlen($name);
        $f=1;
        $error_name1 = "";
        $error_name2 = "";
        for($i = 0;$i<$num;$i++){
            if(($name[$i]<"A" || ($name[$i]>"Z" && $name[$i]<"a") || $name[$i]>"z"))$f=0;
        }
        if($f==0) {
            $error_name1 = " style='color: red;' ";
            $error_name2 = "(a-z or A-Z)";
        }
        else{
            $error_name1 = " style='color: rgb(32, 179, 32);' ";
            $good++;
        }
    }
    else{
        $error_name1 = " style='color: red;' ";
        $error_name2 = "Имя уже занято";
    }
}
else {
    $name = "";
    $error_name1 = " style='color: red;' ";
    $error_name2 = "Введите имя";
}

if(!empty($_GET["password"])){
    $password = htmlentities($_GET["password"]);
    $error_password1 = " style='color: rgb(32, 179, 32);' ";
    $error_password2 = "";
    $good++;
}
else{
    $password="";
    $error_password1 = " style='color: red;' ";
    $error_password2 = "Введите пароль";
}

if(!empty($_GET["email"])){
    $email = htmlentities($_GET["email"]);
    $num = strlen($email);
    $f=0;
    $error_email1="";
    $error_email2="";
    for($i=0;$i<$num;$i++){
        if($email[$i]=="@" && $i>0 && $i<$num-1)$f++;
    }
    if($f!=1){
        $error_email1=" style='color: red;' ";
        $error_email2 = "( ___@___ )";
    }
    else{
        $error_email1 = " style='color: rgb(32, 179, 32);' ";
        $good++;
    }
}
else{
    $email = "";
    $error_email1=" style='color: red;' ";
    $error_email2 = "( ___@___ )";
}

if(!empty($_GET["date"])){
    $date = htmlentities($_GET["date"]);
    $error_date = " style='color: rgb(32, 179, 32);' ";
    $good++;
}
else{
    $date = "";
    $error_date = " style='color: red;' ";
}


if(!empty($_GET["radio1"])){
    $gen = $_GET["radio1"];
    if($gen=="m"){
        $check_gen1="checked";
        $check_gen2="";
    }
    else{
        $check_gen1="";
        $check_gen2="checked";
    }
}else{
    $check_gen1="checked";
    $check_gen2="";
}

if(!empty($_GET["radio2"])){
    $lim = $_GET["radio2"];
    $check_lim = array("","","","","");
    for($i=1;$i<=4;$i++){
        if($i==$lim)$check_lim[$i]="checked";
        else $check_lim[$i]="";
    }
}else{
    $check_lim = array("","","","checked");
}

if(!empty($_GET["textarea1"])){
    $text = $_GET["textarea1"];
}else{
    $text = "";
}

$sel="0";
if(isset($_GET["select"])){
    $sel=" ";
    foreach ($_GET["select"] as $valve){
    $sel .= $valve;
}
$sel = (int)$sel;
}

if(!empty($_GET["checkbox"])){
    $error_check = " style='color: rgb(32, 179, 32);' ";
    $good++;
}
else{
    $error_check=" style='color: red;' ";
}



if($good==5){
    $stmt = $db->prepare("INSERT INTO info (name, password, email, date, gender, limbs, power_code, biography) VALUES (:f_name, :f_password, :f_email, :f_date, :f_gen, :f_lim, :f_powers, :f_biograp)");
    $stmt->bindParam(':f_name', $name);
    $stmt->bindParam(':f_password', $password);
    $stmt->bindParam(':f_email', $email);
    $stmt->bindParam(':f_date', $date);
    $stmt->bindParam(':f_gen', $gen);
    $stmt->bindParam(':f_lim', $lim);
    $stmt->bindParam(':f_powers', $sel);
    $stmt->bindParam(':f_biograp', $text);
    $stmt->execute();

    header ('Location: registration_ok.php');

}
else{
    $answer_style =" style='color: red;' ";
    $answer = "Исправте ошибки";
}
}




$result='<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style1.css">
    <title>MY site</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script>
        function back(){
            window.location.href = "index.php";
            return false;
        }

        function Master(){
            window1.onclick=back;
            return false;
        }

        document.addEventListener("DOMContentLoaded",Master);
    </script>
</head>
<body>
    <div class="main">
        <div id="window1" class="window">
            <div class="back_window">НАЗАД</div>
            <div class="line"></div>
        </div>
        <div id="window2" class="window">
            <form id="form" method="GET" action="registration.php">
                    <label' . $error_name1 . '>Имя: 
                        <input  id="name" name="name" value=' . $name. '><br>'. $error_name2 .'
                    </label><br><br>

                    <label'.$error_password1.'>Пароль: 
                        <input  id="password" name="password" value='.$password.'><br>'.$error_password2.'
                    </label><br><br>
    
                    <label' . $error_email1 . '>E-mail:
                        <input  type="text" id="email" name="email" value=' . $email .'><br>'. $error_email2 .'
                    </label><br><br>

                    <lable' . $error_date . '>Дата рождения:
                        <input type="date" name="date" value=' . $date . '><br><br>
                    </lable>
                    
                    Пол:
                    <label> Мужчина
                        <input name="radio1" type="radio" value="m"'.$check_gen1.'>
                    </label>
                    <label> Женщина
                        <input name="radio1" type="radio" value="w"'.$check_gen2.'>
                    </label><br><br>
    
                    Кол-во конечностей:
                    <label><input name="radio2" type="radio" value="1"'.$check_lim[1].'> 1</label>
                    <label><input name="radio2" type="radio" value="2"'.$check_lim[2].'> 2</label>
                    <label><input name="radio2" type="radio" value="3"'.$check_lim[3].'> 3</label>
                    <label><input name="radio2" type="radio" value="4"'.$check_lim[4].'> 4</label><br><br>
                    
                    <label>Сверхспособности:<br>
                        <select name="select[]" size="3" multiple>
                            <option value="1">immortality</option>
                            <option value="2">passing through walls</option>
                            <option value="3">levitation</option>
                        </select>
                    </label><br><br>
                    
                    <label>Биография:<br>
                        <textarea cols="40" rows="3" name="textarea1">'.$text.'</textarea>
                    </label><br><br>
                    
                    <label ><div'. $error_check .'>Я согласен со всем:</div>
                        <input id="checkbox" name="checkbox" type="checkbox" value="ok">
                    </label><br><br>
                    
                    <input type="submit" value="Отправить"><br><br>
                    <div'. $answer_style .'>' . $answer . '</div>
                </form>
            <div class="line"></div>
        </div>
    </div>
</body>
</html>';

echo "$result";
?>
