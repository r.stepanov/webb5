<?php
session_start();
unset($_SESSION["count1"]);
if (isset($_COOKIE["now"])){
    $name = $_COOKIE["now"];
    setcookie("now",$name,time()+300);
}
else{
    setcookie("now",$name,time()+0);
    header ('Location: enter_again.php');
    exit;
}

$result='<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="style1.css">
    <title>MY site</title>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script>
        function back(){
            window.location.href = "index.php";
            return false;
        }

        function info(){
            window.location.href = "profile_info.php";
            return false;
        }

        function change(){
            window.location.href = "profile_change1.php";
            return false;
        }

        function Master(){
            back1.onclick=back;
            window2.onclick=info;
            window3.onclick=change;
            return false;
        }

        document.addEventListener("DOMContentLoaded",Master);
    </script>
</head>
<body>
    <div class="main">
        <div id="window1" class="prof">
            <div class="prof_name">
                <div class="name">'.$name.'</div>
                <div id="back1" class="exit">ВЫЙТИ</div>
            </div>
            <div class="line"></div>
        </div>
        <div id="window2" class="window3">
            <div class="big-text">Показать данные</div>
            <div class="line"></div>
        </div>
        <div id="window3" class="window3">
            <div class="big-text">Изменить данные</div>
            <div class="line"></div>
        </div>
    </div>
</body>
</html>';

echo "$result";
?>
